export const color = [
    "white",
    "black",
    "red",
    "marun",
    "being",
    "pink",
    "green",
    "yellow",
]

export const filters = [
    {
        id: "color",
        name: "Color",
        options: [
            {value: 'white', label: 'white'},
            {value: 'beige', label: 'beige'},
            {value: 'blue', label: 'blue'},
            {value: 'brown', label: 'brown'},
            {value: 'green', label: 'green'},
            {value: 'purple', label: 'purple'},
            {value: 'yellow', label: 'yellow'},
        ]
    },
    {
        id: "size",
        name: "Size",
        options: [
            {value: 'S', label: 'S'},
            {value: 'M', label: 'M'},
            {value: 'L', label: 'L'},
        ]
    },

]

export const singleFilter = [
    {
        id: 'price',
        name: 'Price',
        options: [
            {value: '159-399', label: '159 руб. - 399 руб.'},
            {value: '399-999', label: '399 руб. - 999 руб.'},
            {value: '999-1999', label: '999 руб. - 1999 руб.'},
            {value: '1999-2999', label: '1999 руб. - 2999 руб.'},
            {value: '3999-4999', label: '3999 руб. - 4999 руб.'},
        ]
    },
    {
        id: 'discount',
        name: 'Discount Range',
        options: [
            {value: '10', label: '10% и выше'},
            {value: '20', label: '20% и выше'},
            {value: '30', label: '30% и выше'},
            {value: '40', label: '40% и выше'},
            {value: '50', label: '50% и выше'},
            {value: '60', label: '60% и выше'},
            {value: '70', label: '70% и выше'},
            {value: '80', label: '80% и выше'},
        ]
    },
    {
        id: 'stock',
        name: 'Availability',
        options: [
            {value: 'in_stock', label: 'In stock'},
            {value: 'out_of_stock', label: 'Out of Stock'},
        ]
    }
]
