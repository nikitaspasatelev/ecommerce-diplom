import { Button, IconButton } from "@mui/material";
import React from "react";
import RemoveCircleOutlineIcon from "@mui/icons-material/RemoveCircleOutline";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";

const CartItem = () => {
  return (
    <div className="p-5 shadow-lg border rounded-md">
      <div className="flex items-center">
        <div className="w-[5rem] h-[5rem] lg:w-[9rem] lg:h-[9rem]">
          <img
            className="w-full h-full object-cover object-top"
            src="https://images.unsplash.com/photo-1617113930975-f9c7243ae527?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2000&q=80"
            alt=""
          />
        </div>
        <div className="ml-5 space-y-1">
          <p className="font-semobold">Men Slim Rise Black Jeans</p>
          <p className="opacity-70">Size: L, White</p>
          <p className="mt-2 opacity-70">Seller: Crist Fashion</p>

          <div className="flex space-x-5 items-center text-gray-900 mt-6">
            <p className="font-semibold text-gray-900">199 руб</p>
            <p className="opacity-50 line-through">211 руб</p>
            <p className="text-green-600 font-semibold">5%</p>
          </div>
        </div>
      </div>
      <div className="lg:flex items-center lg:space-x-10 pt-4">
        <div className="flex items-center space-x-2">
          <IconButton>
            <RemoveCircleOutlineIcon />
          </IconButton>
          <span className="py-1 px-7 border rounded-sm">3</span>
          <IconButton sx={{ color: "rgb(145, 85, 253)" }}>
            <AddCircleOutlineIcon />
          </IconButton>
        </div>

        <div>
          <Button sx={{ color: "rgb(145, 85, 253)" }}>remove</Button>
        </div>
      </div>
    </div>
  );
};

export default CartItem;
