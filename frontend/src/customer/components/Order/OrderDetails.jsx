import React from "react";
import AddressCard from "../AddressCard/AddressCard";
import OrderTracker from "./OrderTracker";
import { Box, Grid} from "@mui/material";
import { deepPurple } from "@mui/material/colors";
import StarBorderIcon from '@mui/icons-material/StarBorder';

const OrderDetails = () => {
  return (
    <div className="px:5 px-20">
      <div>
        <h1 className="font-semibold text-xl py-7">Delivery Address</h1>
        <AddressCard />
      </div>

      <div className="py-20">
        <OrderTracker activeStep={3} />
      </div>

      <Grid className="space-y-5">
        {[1, 1, 1, 1, 1].map((item) =>  <Grid
          item
          container
          className="shadow-xl rounded-md p-5 border"
          sx={{ alignItems: "center", justifyContent: "space-between" }}
        >
          <Grid item xs={6}>
            <div className="flex items-center space-x-4">
              <img
                className="w-[5rem] h-[5rem] object-cover object-top"
                src="https://images.unsplash.com/photo-1617113930975-f9c7243ae527?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2000&q=80"
                alt="image"
              />

              <div className="space-y-2 ml-5">
                <p className="font-semibold">Men Slim Rise Black Jeans</p>
                <p className="space-x-5 opacity-50 text-xs font-semibold">
                  {" "}
                  <span>Color: pink</span> <span>Size: M</span>{" "}
                </p>
                <p>Seller: linaria</p>
                <p>1099 руб.</p>
              </div>
            </div>
          </Grid>

          <Grid item>
            <Box sx={{color: deepPurple[500]}}>
                
                <StarBorderIcon className="px-2" sx={{fontSize: "2.5rem"}} />
                <span>Rate & Review Product</span>
            </Box>
          </Grid>
        </Grid>)}
       
      </Grid>
    </div>
  );
};

export default OrderDetails;
