import React from 'react'

const AddressCard = () => {
  return (
    <div>
        <div className='space-y-3'>
            <p className='font-semibold'>Raam Kapor</p>
            <p>Mumbai, gokul gham market, 40001</p>
            <div className='space-y-1'>
                <p className='font-semibold'>Phone Number</p>
                <p>9222222222</p>
            </div>
        </div>
    </div>
  )
}

export default AddressCard
