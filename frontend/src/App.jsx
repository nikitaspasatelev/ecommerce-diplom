import { Route, Routes, useLocation } from "react-router-dom";
import "./App.css";
import Footer from "./customer/Footer/Footer";
import Cart from "./customer/components/Cart/Cart";
import Checkout from "./customer/components/Checkout/Checkout";
import Navigation from "./customer/components/Navigation/Navigation";
import Order from "./customer/components/Order/Order";
import OrderDetails from "./customer/components/Order/OrderDetails";
import ProductDetails from "./customer/components/ProducrDetails/ProductDetails";
import Product from "./customer/components/Product/Product";
import HomePage from "./customer/pages/HomePage/HomePage";
import CustomerRoutes from "./Routers/CustomerRoutes";
import { useEffect } from "react";


function App() {
  const location = useLocation();
  useEffect(() => {
    fetch('http://localhost:8081/api/products')
    .then(res => res.json())
    .then(data => console.log(data))
    .catch(err => console.log(err))
  })
  return (
    <div className="App">
      <Routes location={location} key={location.pathname}>
        <Route path="/*" element={<CustomerRoutes />}></Route>
      </Routes>

     
    </div>
  );
}

export default App;
