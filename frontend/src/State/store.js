const { legacy_createStore, applyMiddleware, combineReducers } = require("redux");
const { default: thunk } = require("redux-thunk");

const rootReducers = combineReducers({

})

export const store = legacy_createStore(rootReducers, applyMiddleware(thunk))
