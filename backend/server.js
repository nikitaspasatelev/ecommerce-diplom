const express = require("express");
const mysql = require("mysql");
const cors = require("cors");
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser')

const app = express();
app.use(cors());
app.use(express.json())

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "ecommerce_lomoda",
});

app.get("/api", (req, res) => {
  return res.json("From backend side");
});

app.get("/api/products", (req, res) => {
  const sql = "SELECT * FROM products";
  db.query(sql, (err, data) => {
    if (err) return res.json(err);
    return res.json(data);
  });
});
app.post("/api/login", (req, res) => {
  const sql = "SELECT * FROM users WHERE `email` = ? AND `password` = ? ";

  const salt = bcrypt.genSaltSync(15)
  const password = bcrypt.hashSync(req.body.password, salt)

  db.query(sql, [ req.body.email, password ], (err, data) => {
    if (err) return res.json(err);
    if (data.length > 0) {
      const id = data[0].id;
      const token = jwt.sign({id}, "jwt_secret_key", {expiresIn: 300});
      return res.json({Login: true, token, data});
    } else {
      return res.json("Failed");
    }
  });
});
app.post("/api/signup", (req, res) => {
  const sql = "INSERT INTO users (`name`, `email`, `password`) VALUES (?)";

  const salt = bcrypt.genSaltSync(15)
  const password = bcrypt.hashSync(req.body.password, salt) 
  const values = [
    req.body.name,
    req.body.email,
    password,
  ]

  db.query(sql, [values], (err, data) => {
    if (err) return res.json(err);
    return res.json(data);
  });
});
app.post("/api/add", (req, res) => {
  const sql = "INSERT INTO users (`name`, `email`, `password`) VALUES (?)";

  const values = [
    req.body.name,
    req.body.email,
    req.body.password,
  ]

  db.query(sql, [values], (err, data) => {
    const token = jwt.sign({id}, "jwt_secret_key", {expiresIn: 300});
    if (err) return res.json(err);
    return res.json(data);
  });
});

app.listen(8081, () => {
  console.log("listening");
});
